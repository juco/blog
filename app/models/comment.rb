class Comment < ActiveRecord::Base
  belongs_to :post

  validates :post_id, presence: true
  validated :body, presence: true
end
